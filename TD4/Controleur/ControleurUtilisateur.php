<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('utilisateur/liste.php', ["utilisateurs" => $utilisateurs]);//"redirige" vers la vue
    }

    public static function afficherDetail(): void{
        if (isset($_GET['login'])) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
            if($utilisateur){
                ControleurUtilisateur::afficherVue('utilisateur/detail.php', ["utilisateur" => $utilisateur]);
            }
            else{
                ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
            }

        } else {
            ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation(): void{
        require'../vue/utilisateur/formulaireCreation.php';
    }

    public static function creerDepuisFormulaire (): void{
        $utilisateur =  new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        $utilisateur->ajouter();
        self::afficherListe();
    }

}
?>
