<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
        /*8.2
       $nom = "Bussiere";
       $prenom = "Andréa";
       $login="bussierea";

        echo "<p> ModeleUtilisateur $nom $prenom de login $login </p>"*/

        /*ex 8.3
        $utilisateur =[
                'nom' => 'Bussiere',
                'prenom' => 'Andrea',
                'login' => 'bussierea'
        ];
        var_dump($utilisateur);
        echo "<p> ModeleUtilisateur {$utilisateur['nom']} {$utilisateur['prenom']} de login {$utilisateur['login']} </p>" */

        /*ex 8.4
        $utilisateur1 =[
            'nom' => 'Bussiere',
            'prenom' => 'Andrea',
            'login' => 'bussierea'
        ];

        $utilisateur2 =[
            'nom' => 'Yvonet',
            'prenom' => 'Maelina',
            'login' => 'yvonetm'
        ];

        $tableau[] = $utilisateur2;
        $tableau[] = $utilisateur1;
        var_dump($tableau);

        echo "Liste d'utilisateurs : ";
        for($i =0; $i < count($tableau); $i++){
            echo "<ul><li> {$tableau[$i]['nom']}</li>";
            echo "<li> {$tableau[$i]['prenom']}</li>";
            echo "<li> {$tableau[$i]['login']} </li></ul>";
        }*/

        $tableau1 =[
            'nom' => 'Yvonet',
            'prenom' => 'Maelina',
            'login' => 'yvonetm'
        ];

        if(empty($tableau)){
            echo "Il n'y a aucun utilisateur";
        }
        else{
            echo "{$tableau['nom']}";
        }


        // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
         // $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          //echo $texte;
        ?>
    </body>
</html> 