<?php
require_once ("formulaireCreationTrajet.html");
require_once ("Trajet.php");

$date = new DateTime($_GET["date"]);
$datefin = $date ->format('Y-m-d');

$util = Utilisateur::recupererUtilisateurParLogin($_GET["conducteurLogin"]);
$trajet = new Trajet(null, $_GET["depart"], $_GET["arrivee"], $date, $_GET["prix"],$util, isset($_GET["nonFumeur"]));
$trajet ->ajouter();
echo $trajet;
