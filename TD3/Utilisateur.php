<?php
require_once "ConnexionBaseDeDonnees.php";

class Utilisateur
{
    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);
    }

    /**
     * @return mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(mixed $prenom): void
    {
        $this->prenom = $prenom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString()
    {
        echo $this->getNom() . " ";
        echo $this->getPrenom() . " ";
        echo $this->getLogin() . " ";
        return "";
    }


//$ut = new Utilisateur("mirmanq", "Mirman", "Quentin");
//$appel = $ut->__toString();

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        $util = new Utilisateur($utilisateurFormatTableau["login"], $utilisateurFormatTableau["nom"], $utilisateurFormatTableau["prenom"]);
        return $util;
    }

    public static function getUtilisateurs(): array
    {
        $sql = "SELECT * FROM utilisateur";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
        $utilisateurFormatTableau = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);

        $tableau = [];
        foreach ($utilisateurFormatTableau as $utilisateur) {
            $tableau[] = self::construireDepuisTableauSQL($utilisateur);
        }
        var_dump($tableau);
        return $tableau;
    }

    public static function recupererUtilisateurParLogin(string $login): ?Utilisateur
    {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if($utilisateurFormatTableau == null){
            echo "null";
            return null;
        }
        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter () : void{
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:loginTag, :nomTag, :prenomTag)";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom,
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }


}

?>