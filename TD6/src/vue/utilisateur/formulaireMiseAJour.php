<!DOCTYPE html>
<body>
<form method="GET" action="controleurFrontal.php">
    <input type='hidden' name='action' value='mettreAJour'>
    <fieldset>
        <legend>Mise à jour de l'utilisateur : <?php /** @type Utilisateur $utilisateur */

            use App\Covoiturage\Modele\DataObject\Utilisateur;

            echo $utilisateur->getLogin() ?></legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field"  type="text" name = 'login' value =
            "<?php echo $utilisateur->getLogin()?> " readonly = "readonly" </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Leblanc" name="nom" id="nom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Juste" name="prenom" id="prenom_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>

</body>
