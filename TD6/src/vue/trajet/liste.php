<!DOCTYPE html>
<body>
<?php
/** @var Trajet[] $trajets */

use App\Covoiturage\Modele\DataObject\Trajet;

foreach ($trajets as $trajet){
    $idHTML = htmlspecialchars($trajet->getId());
    echo '<p>Trajet de id <a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' . rawurlencode($trajet->getId()) . '">' . $idHTML . '</a>.</p>';
    echo '<p>Modifier le trajet <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&login=' . rawurlencode($trajet->getId()) . '">' . $idHTML . '</a>.</p>';
}
?>
<a href= controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation target = "_blank"> Créer un trajet</a>
<a href= controleurFrontal.php?controleur=trajet&action=afficherFormulaireSuppression target = "_blank"> Supprimer un trajet</a>
</body>

