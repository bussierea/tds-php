<!DOCTYPE html>
<body>
<form method="GET" action="controleurFrontal.php">
    <input type='hidden' name='action' value='mettreAJour'>
    <input type ='hidden' name = 'controleur' value = 'trajet'>
    <fieldset>
        <legend>Mise à jour du trajet : <?php /** @type Trajet $trajet */

            use App\Covoiturage\Modele\DataObject\Trajet;

            echo $trajet->getId() ?></legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="id_id">Id&#42;</label>
            <input class="InputAddOn-field"  type="text" name = 'id' value =
            "<?php echo $trajet->getId()?> " readonly = "readonly"
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Marseille" name="depart" id="depart_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivee&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Nîmes" name="arrivee" id="arrivee_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" placeholder="Ex : JJ/MM/AAAA" name="date" id="date_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : 30" name="prix" id="prix_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteur_id">Conducteur&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="conducteurLogin" id="conducteur_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non fumeur ?&#42;</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="Ex : 1" name="nonFumeur" id="nonFumeur_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>

</body>

