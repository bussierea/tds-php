<!DOCTYPE html>
<body>
<form method="GET" action="controleurFrontal.php">
    <input type='hidden' name='action' value='creerDepuisFormulaire'>
    <input type ='hidden' name = 'controleur' value = 'trajet'>
    <fieldset>
        <legend>Mon formulaire :</legend>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Paris" name="depart" id="depart_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivee&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Londres" name="arrivee" id="arrivee_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" placeholder="Ex : JJ/MM/AAAA" name="date" id="date_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : 15" name="prix" id="prix_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteur_id">Conducteur&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : potterh" name="conducteurLogin" id="conducteur_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="fumeur_id">Non fumeur ?&#42;</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="Ex : 1" name="nonFumeur" id="nonFumeur_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>

</body>
