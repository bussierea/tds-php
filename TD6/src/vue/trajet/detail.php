<!DOCTYPE html>
<body>
<?php
/**@var Trajet $trajet */
/**@var Utilisateur $passager */


use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;

$idHTML = htmlspecialchars($trajet->getId());
$departHTML = htmlspecialchars($trajet -> getDepart());
$arriveeHTML = htmlspecialchars($trajet -> getArrivee());
$dateHTML = htmlspecialchars($trajet -> getDate()->format("Y-m-d"));
$prixHTML = htmlspecialchars($trajet -> getPrix());
$conducteurHTML = htmlspecialchars($trajet -> getConducteur()->getNom());
$nonFumeurHTML = htmlspecialchars($trajet -> isNonFumeur());

echo ($departHTML . " ");
echo ($arriveeHTML . " ");
echo ($idHTML . " ");
echo(" $conducteurHTML $dateHTML $prixHTML $nonFumeurHTML");
foreach ($trajet->getPassagers() as $passager){
echo $passager->getNom();
}
?>
</body>

