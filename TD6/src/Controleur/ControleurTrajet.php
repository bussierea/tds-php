<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\DataObject\Trajet as Trajet;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet
{

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $trajets = (new \App\Covoiturage\Modele\Repository\TrajetRepository)->recuperer(); //appel au modèle pour gérer la BD
        ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "trajet/liste.php", "trajets" => $trajets]);
        //ControleurUtilisateur::afficherVue('utilisateur/liste.php', ["utilisateurs" => $utilisateurs]);//"redirige" vers la vue
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur = "")
    {
        ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "trajet/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['id'])) {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
            if ($trajet) {
                ControleurTrajet::afficherVue('vueGenerale.php', ["titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/detail.php", "trajet" => $trajet]);
            } else {
                ControleurTrajet::afficherErreur("Trajet inexistant");
            }

        } else {
            ControleurTrajet::afficherErreur("Pas d'id donné");
        }
    }
        public static function supprimer (){
        $id = $_GET["id"];
        (new TrajetRepository)->supprimer($id);
        $trajets= (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetSupprime.php", "trajets" => $trajets]);
    }
    public static function afficherFormulaireSuppression(): void
    {
        require __DIR__ . '/../vue/trajet/formulaireSuppression.php';
    }
    public static function afficherFormulaireCreation(): void
    {
        require __DIR__ . '/../vue/trajet/formulaireCreation.php';
    }
    public static function afficherFormulaireMiseAJour() {
        // Récupération de l'identifiant du trajet depuis la requête (par exemple via GET)
        $idTrajet = $_GET['login'] ?? null; // Assurez-vous que l'ID est passé dans l'URL ou le formulaire

        // Récupération du trajet correspondant
        if ($idTrajet) {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($idTrajet);

            // Vérifiez que le trajet existe
            if ($trajet) {
                // Inclure la vue avec le trajet
                require __DIR__ . '/../vue/trajet/formulaireMiseAJour.php';
            } else {
                // Gestion de l'erreur si le trajet n'existe pas
                echo "Trajet non trouvé.";
            }
        } else {
            // Gestion de l'erreur si l'ID du trajet n'est pas fourni
            echo "ID du trajet non fourni.";
        }
    }

    public static function mettreAJour(){
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets= (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetMisAJour.php", "id"=> $trajet->getId(), "trajets" => $trajets]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
    }

    /**
     * @return void
     */
    /**
     * @return void
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $depart = $tableauDonneesFormulaire["depart"];
        $arrivee = $tableauDonneesFormulaire["arrivee"];
        $date = new DateTime($tableauDonneesFormulaire["date"]);
        $prix = (int)$tableauDonneesFormulaire["prix"];
        $conducteurLogin = $tableauDonneesFormulaire["conducteurLogin"];
        $conducteur = (new UtilisateurRepository())->recupererParClePrimaire($conducteurLogin);
        $nonFumeur = isset($tableauDonneesFormulaire["nonFumeur"]);

        return new Trajet($id, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
    }


}
