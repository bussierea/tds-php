<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees as ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Trajet as Trajet;
use DateTime;
use \PDO as PDO;

class TrajetRepository extends AbstractRepository
{

    public static function recupererTrajets()
    {
    }

    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $date = new DateTime($trajetTableau["date"]);
        $util = (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]);
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            $date, // À changer
            $trajetTableau["prix"],
            $util, // À changer
            $trajetTableau["nonFumeur"], // À changer ?
        );
        $trajet ->setPassagers((new TrajetRepository)->recupererPassagers($trajet));
        return $trajet;
    }
   /* public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/

    private function recupererPassagers(Trajet $trajet) : array {
        $sql = "SELECT login, nom, prenom FROM `utilisateur` u
                join passager p on u.login = p.passagerLogin
                where p.trajetId = :idTag";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $tab = ["idTag" => $trajet ->getId()];
        $pdoStatement->execute($tab);
        $passagerTableau = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);

        $tableau = [];
        foreach ($passagerTableau as $passager) {
            $tableau[] = (new UtilisateurRepository)->construireDepuisTableauSQL($passager);
        }
        return $tableau;
    }

    protected function getNomTable(): string
    {
       return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
       return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format('Y-m-d'),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur()
        );
    }


}