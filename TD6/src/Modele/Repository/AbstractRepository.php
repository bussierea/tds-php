<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\Trajet;
use PDO;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $table = $this->getNomTable();
        $clePrimaire = $this->getNomClePrimaire();

        $colonnes = $this->getNomsColonnes();
        $pairesColonnesPlaceholders = join(", ", array_map(fn($col) => "$col = :{$col}Tag", $colonnes));

        $sql = "UPDATE $table SET $pairesColonnesPlaceholders WHERE $clePrimaire = :clefTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);

        // Ajouter la valeur de la clé primaire dans le tableau des valeurs
        if($objet instanceof Utilisateur){
            $values['clefTag'] = $objet->getLogin();
        }
         elseif ($objet instanceof Trajet){
            $values['clefTag'] = $objet->getId();
         }
        // Exécuter la requête
        $pdoStatement->execute($values);
    }


    public function ajouter(AbstractDataObject $objet): bool
    {
        $table = $this->getNomTable();
        $colonnes = join(", ", $this->getNomsColonnes());
        $placeholders = join(", ", array_map(fn($col) => ":{$col}Tag", $this->getNomsColonnes()));

        $sql = "INSERT INTO $table ($colonnes) VALUES ($placeholders)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        return $pdoStatement->execute($values);
    }

    public function supprimer($valeurClePrimaire): void
    {
        $table = $this->getNomTable();
        $cle = $this ->getNomClePrimaire();
        $sql = "DELETE FROM $table where $cle = :cleTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "cleTag" => $valeurClePrimaire,
        );
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $cle): ?AbstractDataObject
    {
        $table = $this ->getNomTable();
        $clePrimaire = $this -> getNomClePrimaire();
        $sql = "SELECT * from $table WHERE $clePrimaire = :cleTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "cleTag" => $cle,
            //nomdutag => valeur, ...
        );
        $pdoStatement->execute($values);

        $formatTableau = $pdoStatement->fetch();
        if ($formatTableau == null) {
            echo "null";
            return null;
        }
        return $this->construireDepuisTableauSQL($formatTableau);
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $table = $this->getNomTable();
        $sql = "SELECT * FROM $table";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
        $ligneFormatTableau = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);

        $tableau = [];

        foreach ($ligneFormatTableau as $ligne) {
            $tableau[] = $this->construireDepuisTableauSQL($ligne);
        }
        //var_dump($tableau);
        return $tableau;
    }

    protected abstract function getNomTable(): string;
    protected abstract function getNomClePrimaire(): string;
    protected abstract function getNomsColonnes (): array;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

    protected abstract function formatTableauSQL(AbstractDataObject $objet) : array;

}