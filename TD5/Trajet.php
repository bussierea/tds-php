<?php

use Modele\ConnexionBaseDeDonnees;
use Modele\ModeleUtilisateur;

require_once 'ConnexionBaseDeDonnees.php';
require_once 'ModeleUtilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private ModeleUtilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int              $id,
        string            $depart,
        string            $arrivee,
        DateTime          $date,
        int               $prix,
        ModeleUtilisateur $conducteur,
        bool              $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];

    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $date = new DateTime($trajetTableau["date"]);
        $util = ModeleUtilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]);
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            $date, // À changer
            $trajetTableau["prix"],
            $util, // À changer
            $trajetTableau["nonFumeur"], // À changer ?
        );
        $trajet->recupererPassagers();
        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): ModeleUtilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(ModeleUtilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }



    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter () : void{
        $sql = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur) 
            VALUES (:departTag, :arriveeTag, :dateTag, :prixTag, :conducteurLoginTag, :nonFumeurTag)";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "departTag" => $this->depart,
            "arriveeTag" => $this->arrivee,
            "dateTag" => $this->date ->format("Y-m-d"),
            "prixTag" => $this->prix,
            "conducteurLoginTag" => $this ->conducteur->getLogin(),
            "nonFumeurTag" => $this ->nonFumeur ?1:0
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }

    /**
     * @return ModeleUtilisateur[]
     */
    private function recupererPassagers() : array {
        $sql = "SELECT login, nom, prenom FROM `utilisateur` u
                join passager p on u.login = p.passagerLogin
                where p.trajetId = :idTag";

        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $tab = ["idTag" => $this->id];
        $pdoStatement->execute($tab);
        $passagerTableau = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);

        $tableau = [];
        foreach ($passagerTableau as $passager) {
            $tableau[] = ModeleUtilisateur::construireDepuisTableauSQL($passager);
        }
        var_dump($tableau);
        return $tableau;
    }
}
