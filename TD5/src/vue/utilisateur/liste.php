<!DOCTYPE html>
<body>
<?php
/** @var ModeleUtilisateur[] $utilisateurs */

use App\Covoiturage\Modele\ModeleUtilisateur;

foreach ($utilisateurs as $utilisateur){
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    echo '<p>Utilisateur de login <a href="controleurFrontal.php?action=afficherDetail&login=' . (rawurlencode($utilisateur-> getLogin())) . '">' . $loginHTML . '</a>.</p>';
}
?>
 <a href= controleurFrontal.php?action=afficherFormulaireCreation target = "_blank"> Créer un utilisateur</a>
</body>
</html>
