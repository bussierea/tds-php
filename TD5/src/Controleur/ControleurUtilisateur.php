<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;
class ControleurUtilisateur
{

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php", "utilisateurs" => $utilisateurs]);
        //ControleurUtilisateur::afficherVue('utilisateur/liste.php', ["utilisateurs" => $utilisateurs]);//"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['login'])) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
            if ($utilisateur) {
                ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/detail.php","utilisateur" => $utilisateur]);
            } else {
                ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/erreur.php"]);
            }

        } else {
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__. '/../vue/' .$cheminVue; // Charge la vue
    }

    public static function afficherFormulaireCreation(): void
    {
        require __DIR__ . '/../vue/utilisateur/formulaireCreation.php';
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        $utilisateur->ajouter();
        $utilisateurs= ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
    }

}

?>
